<?php

/**
 * If $base_url is not set in settings.php, this *will not work*.
 */

/**
 * Implements hook_init().
 */
function domainpark_init() {
  // We only redirect if the incoming domain is different from the site's usual.
  global $base_url;
  $original_uri = $_SERVER['SCRIPT_URI'];
  $domain = parse_url($original_uri, PHP_URL_HOST);
  if (parse_url($base_url, PHP_URL_HOST) == $domain) {
    return;
  }

  // Load the node ID, if any, associated with the incoming domain.
  // TODO: Allow this to be a field selected in the UI not hardcoded?
  $result = db_query(
    'SELECT entity_type, entity_id FROM field_data_field_domain WHERE field_domain_value = :domain',
		array(':domain' => $domain)
  )->fetchObject();
  if ($result) {
    // The redirect header requires a complete URL.
    $options = array('absolute' => TRUE);
    // entity_uri() needs an entity object, unfortunately, not just an ID.
    // We'll only definitely work for nodes doing it this way.
    $path = $result->entity_type . '/' . $result->entity_id;
    $url = url($path, $options);
    // Do the redirect.
    watchdog('domainredirect', '!original_uri redirected to !path.', array('!original_uri'));
    domainpark_redirect($url);

    // TODO: Add a statistics table to track visits 'to' each domain, if PiWik
    // does not already do that.
  }
}

/**
 * Perform a URL redirect.
 *
 * @param $redirect
 *   An optional URL redirect array.
 *
 * @ingroup redirect_api
 */
function domainpark_redirect($url, $options = array(), $id = NULL) {

  // TODO maybe: Options array can include 'query' to pass along a query string.
  $url = url($url, $options);
  drupal_add_http_header('Location', $url);
  drupal_add_http_header('Status', 301);

  if ($id) {
    // TODO: Remove if we do not need.  Adapted from Redirect module.
    // Add a custom header for the redirect ID so when the redirect is served
    // from the page cache, we can track it.
    drupal_add_http_header('X-Redirect-ID', $id);
  }

  // Redirect module also had this check before drupal_exit:
  // if (!variable_get('redirect_page_cache', 0) || !variable_get('cache', 0) || !drupal_page_is_cacheable() || empty($redirect->cache)) {
  drupal_exit($url);
}
